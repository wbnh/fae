package ch.bwe.fae.type;

import java.util.LinkedList;
import java.util.List;

import org.apache.directory.api.ldap.model.entry.Attribute;
import org.apache.directory.api.ldap.model.entry.DefaultAttribute;
import org.apache.directory.api.ldap.model.entry.DefaultEntry;
import org.apache.directory.api.ldap.model.entry.Entry;
import org.apache.directory.api.ldap.model.entry.Value;
import org.apache.directory.api.ldap.model.exception.LdapException;
import org.apache.directory.api.ldap.model.exception.LdapInvalidAttributeValueException;
import org.apache.directory.api.ldap.model.exception.LdapInvalidDnException;
import org.apache.directory.api.ldap.model.name.Dn;
import org.apache.directory.api.ldap.model.schema.AttributeType;

/**
 * Builder for LDAP entries.
 *
 * @author Benjamin Weber
 *
 */
public class EntryBuilder {

	private Dn dn;
	private List<Attribute> attributes = new LinkedList<>();
	private List<String> unparsedAttributes = new LinkedList<>();

	public EntryBuilder(String dn) throws LdapInvalidDnException {
		this.dn = new Dn(dn);
	}

	public EntryBuilder(String... dn) throws LdapInvalidDnException {
		this.dn = new Dn(dn);
	}

	public EntryBuilder(Dn dn) {
		this.dn = dn;
	}

	public EntryBuilder attribute(String attribute) {
		unparsedAttributes.add(attribute);
		return this;
	}

	public EntryBuilder attribute(String... attribute) {
		for (String string : attribute) {
			unparsedAttributes.add(string);
		}
		return this;
	}

	public EntryBuilder attribute(String name, AttributeType type, byte[]... value)
					throws LdapInvalidAttributeValueException {
		Attribute attribute = new DefaultAttribute(type);
		attribute.add(value);
		attributes.add(attribute);
		return this;
	}

	public EntryBuilder attribute(String name, AttributeType type, String... value)
					throws LdapInvalidAttributeValueException {
		Attribute attribute = new DefaultAttribute(type);
		attribute.add(value);
		attributes.add(attribute);
		return this;
	}

	public EntryBuilder attribute(String name, AttributeType type, Value<?>... value)
					throws LdapInvalidAttributeValueException {
		Attribute attribute = new DefaultAttribute(type);
		attribute.add(value);
		attribute.setUpId(name);
		attributes.add(attribute);
		return this;
	}

	public Entry build() throws LdapException {
		Entry entry = new DefaultEntry(dn, unparsedAttributes.toArray());

		for (Attribute attribute : attributes) {
			entry.add(attribute);
		}

		return entry;
	}
}
