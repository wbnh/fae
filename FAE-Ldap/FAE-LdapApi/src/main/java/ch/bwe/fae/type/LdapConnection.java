package ch.bwe.fae.type;

import java.io.Closeable;
import java.io.IOException;
import java.util.Hashtable;

import javax.naming.AuthenticationException;
import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.directory.InitialDirContext;

import org.apache.directory.api.ldap.model.entry.DefaultModification;
import org.apache.directory.api.ldap.model.entry.Entry;
import org.apache.directory.api.ldap.model.entry.Modification;
import org.apache.directory.api.ldap.model.entry.ModificationOperation;
import org.apache.directory.api.ldap.model.exception.LdapException;
import org.apache.directory.api.ldap.model.exception.LdapInvalidDnException;
import org.apache.directory.api.ldap.model.message.AddRequest;
import org.apache.directory.api.ldap.model.message.AddRequestImpl;
import org.apache.directory.api.ldap.model.message.AddResponse;
import org.apache.directory.api.ldap.model.message.CompareRequest;
import org.apache.directory.api.ldap.model.message.CompareRequestImpl;
import org.apache.directory.api.ldap.model.message.CompareResponse;
import org.apache.directory.api.ldap.model.message.DeleteRequest;
import org.apache.directory.api.ldap.model.message.DeleteRequestImpl;
import org.apache.directory.api.ldap.model.message.DeleteResponse;
import org.apache.directory.api.ldap.model.message.ModifyRequest;
import org.apache.directory.api.ldap.model.message.ModifyRequestImpl;
import org.apache.directory.api.ldap.model.message.ModifyResponse;
import org.apache.directory.api.ldap.model.message.ResultCodeEnum;
import org.apache.directory.api.ldap.model.message.controls.ManageDsaITImpl;
import org.apache.directory.api.ldap.model.name.Dn;
import org.apache.directory.ldap.client.api.LdapNetworkConnection;

import ch.bwe.faa.v1.core.service.ServiceRegistry;

/**
 * Connection to an LDAP server.
 *
 * @author Benjamin Weber
 *
 */
public class LdapConnection implements Closeable {

	private LdapNetworkConnection connection;
	private String applicationName;
	private String host;
	private int port;

	/**
	 * Constructor handling initialization of mandatory fields.
	 * 
	 * @throws LdapException
	 *           if no connection can be established.
	 *
	 */
	public LdapConnection(String address, int port, String user, String password, String applicationName)
					throws LdapException {
		connection = new LdapNetworkConnection(address, port);
		connection.bind(user, password);
		this.applicationName = applicationName;
		this.host = address;
		this.port = port;
	}

	public void addGroup(String groupName) throws LdapException {
		ensureGroupUnitExistence();

		Entry group = createGroup(groupName);

		AddResponse response = connection.add(addRequest(group));
		ResultCodeEnum resultCode = response.getLdapResult().getResultCode();
		if (!ResultCodeEnum.SUCCESS.equals(resultCode)) {
			ServiceRegistry.getLogProxy().error(this, "Request failed: {0}", null, resultCode.getMessage());
			throw new LdapException("Request was not successful. Result code: " + resultCode);
		}
	}

	public void removeGroup(String groupName) throws LdapException {
		String dn = getGroupPath(groupName);

		DeleteResponse response = connection.delete(deleteRequest(dn));
		ResultCodeEnum resultCode = response.getLdapResult().getResultCode();
		if (!ResultCodeEnum.SUCCESS.equals(resultCode)) {
			ServiceRegistry.getLogProxy().error(this, "Request failed: {0}", null, resultCode.getMessage());
			throw new LdapException("Request was not successful. Result code: " + resultCode);
		}
	}

	public void addPerson(String uid, String password, String... groups) throws LdapException {
		ensureUserUnitExistence();

		String dn = getUserPath(uid);

		EntryBuilder builder = new EntryBuilder(dn);
		builder.attribute("objectClass: inetOrgPerson");
		builder.attribute("cn: " + uid);
		// builder.attribute("keyAlgorithm: RSA");
		builder.attribute("sn: " + uid);
		builder.attribute("userPassword: " + password);

		AddResponse response = connection.add(addRequest(builder.build()));
		ResultCodeEnum resultCode = response.getLdapResult().getResultCode();
		if (!ResultCodeEnum.SUCCESS.equals(resultCode)) {
			ServiceRegistry.getLogProxy().error(this, "Request failed: {0}", null, resultCode.getMessage());
			throw new LdapException("Request was not successful. Result code: " + resultCode);
		}

		for (String group : groups) {
			addPersonToGroup(uid, group);
		}
	}

	public void removePerson(String uid) throws LdapException {
		String dn = getUserPath(uid);

		DeleteResponse response = connection.delete(deleteRequest(dn));
		ResultCodeEnum resultCode = response.getLdapResult().getResultCode();
		if (!ResultCodeEnum.SUCCESS.equals(resultCode)) {
			ServiceRegistry.getLogProxy().error(this, "Request failed: {0}", null, resultCode.getMessage());
			throw new LdapException("Request was not successful. Result code: " + resultCode);
		}
	}

	public void addPersonToGroup(String personUid, String groupCn) throws LdapException {
		Modification mod = new DefaultModification(ModificationOperation.ADD_ATTRIBUTE, "member", getUserPath(personUid));

		ModifyRequest request = modifyRequest(getGroupPath(groupCn));
		request.addModification(mod);
		ModifyResponse response = connection.modify(request);
		ResultCodeEnum resultCode = response.getLdapResult().getResultCode();
		if (!ResultCodeEnum.SUCCESS.equals(resultCode)) {
			ServiceRegistry.getLogProxy().error(this, "Request failed: {0}", null, resultCode.getMessage());
			throw new LdapException("Request was not successful. Result code: " + resultCode);
		}
	}

	public void removePersonFromGroup(String personUid, String groupCn) throws LdapException {
		Modification mod = new DefaultModification(ModificationOperation.REMOVE_ATTRIBUTE, "member",
						getUserPath(personUid));

		ModifyRequest request = modifyRequest(getGroupPath(groupCn));
		request.addModification(mod);
		ModifyResponse response = connection.modify(request);
		ResultCodeEnum resultCode = response.getLdapResult().getResultCode();
		if (!ResultCodeEnum.SUCCESS.equals(resultCode)) {
			ServiceRegistry.getLogProxy().error(this, "Request failed: {0}", null, resultCode.getMessage());
			throw new LdapException("Request was not successful. Result code: " + resultCode);
		}
	}

	public void addGroupToGroup(String groupToAdd, String groupCn) throws LdapException {
		Modification mod = new DefaultModification(ModificationOperation.ADD_ATTRIBUTE, "member", getGroupPath(groupToAdd));

		ModifyRequest request = modifyRequest(getGroupPath(groupCn));
		request.addModification(mod);
		ModifyResponse response = connection.modify(request);
		ResultCodeEnum resultCode = response.getLdapResult().getResultCode();
		if (!ResultCodeEnum.SUCCESS.equals(resultCode)) {
			ServiceRegistry.getLogProxy().error(this, "Request failed: {0}", null, resultCode.getMessage());
			throw new LdapException("Request was not successful. Result code: " + resultCode);
		}
	}

	public void removeGroupFromGroup(String groupToRemove, String groupCn) throws LdapException {
		Modification mod = new DefaultModification(ModificationOperation.REMOVE_ATTRIBUTE, "member",
						getGroupPath(groupToRemove));

		ModifyRequest request = modifyRequest(getGroupPath(groupCn));
		request.addModification(mod);
		ModifyResponse response = connection.modify(request);
		ResultCodeEnum resultCode = response.getLdapResult().getResultCode();
		if (!ResultCodeEnum.SUCCESS.equals(resultCode)) {
			ServiceRegistry.getLogProxy().error(this, "Request failed: {0}", null, resultCode.getMessage());
			throw new LdapException("Request was not successful. Result code: " + resultCode);
		}
	}

	public void changePassword(String uid, String newPassword) throws LdapException {
		Modification mod = new DefaultModification(ModificationOperation.REPLACE_ATTRIBUTE, "userPassword", newPassword);

		ModifyRequest request = modifyRequest(getUserPath(uid));
		request.addModification(mod);
		ModifyResponse response = connection.modify(request);
		ResultCodeEnum resultCode = response.getLdapResult().getResultCode();
		if (!ResultCodeEnum.SUCCESS.equals(resultCode)) {
			ServiceRegistry.getLogProxy().error(this, "Request failed: {0}", null, resultCode.getMessage());
			throw new LdapException("Request was not successful. Result code: " + resultCode);
		}
	}

	public boolean authenticatePerson(String uid, String password) {

		Hashtable<String, String> namingParameters = new Hashtable<>();
		namingParameters.put(Context.SECURITY_PRINCIPAL, getUserPath(uid));
		namingParameters.put(Context.SECURITY_CREDENTIALS, password);
		namingParameters.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
		namingParameters.put(Context.PROVIDER_URL, "ldap://" + host + ":" + port + "/ou=system");

		try {
			new InitialDirContext(namingParameters);
			return true;
		} catch (AuthenticationException e) {
			ServiceRegistry.getLogProxy().info(this, "Could not authenticate user {0}", uid);
			return false;
		} catch (NamingException e) {
			ServiceRegistry.getLogProxy().error(this, "Could not authenticate user {0}", e, uid);
			return false;
		}
	}

	public boolean doesGroupExist(String groupCn) throws LdapException {
		return connection.exists(getGroupPath(groupCn));
	}

	public boolean doesUserExist(String uid) throws LdapException {
		return connection.exists(getUserPath(uid));
	}

	public boolean isUserInGroup(String userUid, String groupCn) throws LdapException {

		CompareRequest request = compareRequest(getGroupPath(groupCn), "member", getUserPath(userUid));
		CompareResponse response = connection.compare(request);

		return response.isTrue();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void close() throws IOException {
		if (connection != null) {
			connection.close();
		}
	}

	public boolean isOpen() {
		return connection != null && connection.isConnected();
	}

	private String getGroupPath(String groupName) {
		StringBuilder builder = new StringBuilder();
		builder.append("cn=");
		builder.append(groupName);
		builder.append(",");
		builder.append(getLdapGroupUnitPath());
		return builder.toString();
	}

	private String getUserPath(String uid) {
		StringBuilder builder = new StringBuilder();
		builder.append("uid=");
		builder.append(uid);
		builder.append(",");
		builder.append(getLdapUserUnitPath());
		return builder.toString();
	}

	private boolean doesGroupUnitExist() throws LdapException {
		return connection.exists(getLdapGroupUnitPath());
	}

	private boolean doesUserUnitExist() throws LdapException {
		return connection.exists(getLdapUserUnitPath());
	}

	private AddRequest addRequest(Entry entry) {
		AddRequest request = new AddRequestImpl();
		request.setEntry(entry);
		request.addControl(new ManageDsaITImpl());
		return request;
	}

	private DeleteRequest deleteRequest(String dn) throws LdapInvalidDnException {
		DeleteRequest request = new DeleteRequestImpl();
		request.setName(new Dn(dn));
		request.addControl(new ManageDsaITImpl());
		return request;
	}

	private ModifyRequest modifyRequest(String dn) throws LdapInvalidDnException {
		ModifyRequest request = new ModifyRequestImpl();
		request.setName(new Dn(dn));
		request.addControl(new ManageDsaITImpl());
		return request;
	}

	private CompareRequest compareRequest(String dn, String attribute, String value) throws LdapInvalidDnException {
		CompareRequest request = new CompareRequestImpl();
		request.setName(new Dn(dn));
		request.setAttributeId(attribute);
		request.setAssertionValue(value);
		return request;
	}

	private String getLdapGroupUnitPath() {
		StringBuilder builder = new StringBuilder();

		builder.append("ou=");
		builder.append(applicationName);
		builder.append(",ou=groups,ou=system");

		return builder.toString();
	}

	private String getLdapUserUnitPath() {
		StringBuilder builder = new StringBuilder();

		builder.append("ou=");
		builder.append(applicationName);
		builder.append(",ou=users,ou=system");

		return builder.toString();
	}

	private Entry createGroup(String groupName) throws LdapException {
		String dn = getGroupPath(groupName);

		EntryBuilder builder = new EntryBuilder(dn);
		builder.attribute("objectClass: groupOfNames");
		builder.attribute("cn: " + groupName);
		builder.attribute("member: uid=admin,ou=system");

		return builder.build();
	}

	private Entry createGroupUnit() throws LdapException {
		StringBuilder dnBuilder = new StringBuilder();
		dnBuilder.append(getLdapGroupUnitPath());

		EntryBuilder builder = new EntryBuilder(dnBuilder.toString());
		builder.attribute("objectClass: organizationalUnit");

		return builder.build();
	}

	private Entry createUserUnit() throws LdapException {
		StringBuilder dnBuilder = new StringBuilder();
		dnBuilder.append(getLdapUserUnitPath());

		EntryBuilder builder = new EntryBuilder(dnBuilder.toString());
		builder.attribute("objectClass: organizationalUnit");
		builder.attribute("objectClass: top");

		return builder.build();
	}

	private void ensureGroupUnitExistence() throws LdapException {
		if (!doesGroupUnitExist()) {

			Entry group = createGroupUnit();

			AddResponse response = connection.add(addRequest(group));
			ResultCodeEnum resultCode = response.getLdapResult().getResultCode();
			if (!ResultCodeEnum.SUCCESS.equals(resultCode)) {
				throw new LdapException("Add request was not successful. Result code: " + resultCode);
			}
		}
	}

	private void ensureUserUnitExistence() throws LdapException {
		if (!doesUserUnitExist()) {

			Entry group = createUserUnit();

			AddResponse response = connection.add(addRequest(group));
			ResultCodeEnum resultCode = response.getLdapResult().getResultCode();
			if (!ResultCodeEnum.SUCCESS.equals(resultCode)) {
				throw new LdapException("Add request was not successful. Result code: " + resultCode);
			}
		}
	}

}
