package ch.bwe.fae.type;

/**
 * Test class to test the ldap API.
 *
 * @author Benjamin Weber
 *
 */
public class Test {

	@org.junit.Test
	public void test() throws Exception {

		try (LdapConnection connection = new LdapConnection("localhost", 10389, "uid=admin,ou=system", "secret",
						"FAE-Test")) {
			connection.addGroup("TestGroup");
			connection.addPerson("person", "password", "TestGroup");

			connection.authenticatePerson("person", "password");

			System.out.println(connection.isUserInGroup("person", "TestGroup"));

			connection.changePassword("person", "newPassword");

			connection.authenticatePerson("person", "newPassword");

			connection.removePersonFromGroup("person", "TestGroup");

			connection.removePerson("person");
			connection.removeGroup("TestGroup");
		}
	}
}
